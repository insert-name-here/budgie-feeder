require('dotenv').config();
let userId = process.env.BUDGIE_USER_ID;
let authKey = process.env.BUDGIE_AUTH_KEY;
let budgieApi = `https://x0plygq8yd.execute-api.af-south-1.amazonaws.com/prod/${userId}/transactions`;

let accountsConfig = require('./accounts.json');

const fetch = require('node-fetch');
const fs = require('fs');
const moment = require('moment');

// set this first in case the script is run at the stroke of midnight
let today = moment().format('YYYYMMDD');

for (let accountId in accountsConfig) {
    let account = accountsConfig[accountId];
    if (!account.budgieAccountId) {
        console.log(`${accountId} does not have a "budgieAccountId" set, skipping.`);
        continue;
    }
    let holdingFilename = `./holding-${account.budgieAccountId}.json`;
    if (!fs.existsSync(holdingFilename)) {
        console.log(`holding file for account ${accountId} not found, skipping.`);
        continue;
    }
    let holding = require(holdingFilename);
    console.log(`uploading transactions from ${accountId} to ${account.budgieAccountId}...`);

    // upload holding.js transactions
    // TODO upload limited amount of transactions (batches of 50, maybe?)
    fetch(budgieApi, {
        method: 'post',
        body:    JSON.stringify(holding),
        headers: {
            'Authorization': `Bearer ${authKey}`,
            'Content-Type': 'application/json',
        },
    })
    .then(res => res.json())
    .then((response) => {
        console.log(JSON.stringify(response, null, 4));
        if (response.success) {
            // remove any transactions from before today's date
            // to avoid interfering with the iterator we do this by recreating
            // the holding object and copying only today's transactions
            let todayHolding = {};
            todayHolding[account.budgieAccountId] = [];
            todayHoldingTransactions = todayHolding[account.budgieAccountId];
            for (let t in holding[account.budgieAccountId]) {
                let transaction = holding[account.budgieAccountId][t];
                if (transaction.transactionDate == today) {
                    todayHoldingTransactions.push(transaction);
                }
            }
            let holdingFilename = `./holding-${account.budgieAccountId}.json`;
            fs.writeFileSync(holdingFilename, JSON.stringify(todayHolding, null, 4));
        }
    });
}