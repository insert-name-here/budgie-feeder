# budgie-feeder

Retrieve, review and upload transactions to the [budgie](https://gitlab.com/fisher.adam.online/budgie) project

## Prerequisites

Run `npm install` to install script dependencies.

## Sensitive Data

Sensitive data (eg. credentials, account numbers) are to be stored in a `.env` file that must never be checked in. Copy the `sensitives-template` file to `.env` and complete it manually.

Included in this file is `DEFAULT_MASKING`, which can be set to `TRUE` or `FALSE` depending on whether you would prefer your transactions to be masked by default.

## Account Configuration

The `accounts.json` configuration file will be created when adding or loading accounts.

### Investec Accounts

To load Investec accounts into the configuration file, run `node load-investec-accounts.js`.

Generate a budgie account for each account you intend to connect to budgie, then add the `budgieAccountId` key to the record in `config.json` as follows:

```json
    ...
    '<INVESTEC_ACCOUNT_ID>': {
        bank: "investec",
        accountId: '<INVESTEC_ACCOUNT_ID>',
        accountNumber: '<INVESTEC_ACCOUNT_NUMBER>',
        accountName: 'Mr Your Name Here',
        referenceName: 'Mr Your Name Here',
        productName: 'Private Bank Account',
        budgieAccountID: '<BUDGIE_ACCOUNT_ID>'
    },
    ...
```

### Other Bank Accounts

To add other bank accounts, run `add-account.js` with the bank name, bank account number and its corresponding budgie account id (run with `-h` or `--help` for usage).

## Operation

### Import Transactions

Important note: for budgie, positive amounts are treated as debits and negative amounts as credits. The following is the required upload format:

```json
{
    "<budgie account uuid>": [
        {
            "transactionId": "5b48d76f-3059-4799-82bf-169c05c83eda",
            "transactionDate": "20210219",
            "description": "Advance On Garden C / 479012*0780  16 Feb",
            "amount": 1000,
            "currency": "ZAR",
            "labels": [],
            "masked": true
        },
        ...
    ]
}
```

Zero-value transactions are not accepted by budgie, zero-amount transactions will therefore be ignored.

#### Import Investec Transactions

To import transactions since the latest upload, run `node import-investec.js`.

The transactions will be stored in `holding-<ACCOUNT_ID>.json`, and the last day's transactions will remain there after upload to prevent transactions from being uploaded multiple times with different transaction IDs.

#### Import FNB Transactions

Only CSV file imports are supported. To convert a PDF Credit Card Statement, run `node fnb-to-csv.js` and manually clean up the resulting `.csv` file before importing.

**NOTE**: the rows up to and including the header are formatted to be read by the `import-fnb.js` script, and each row must be prefixed by a `5`.

**NOTE**: to preserve quotations, any text that includes quotation characters must be surrounded by different quotation characters eg. "It's fine." vs 'Paid for "Special Services"' (only ", ' and ` supported)

To import transactions on a CSV file, run `node import-fnb.js` (run with `-h` or `--help` for usage).

### Editing Transactions

Transactions can be edited in `holding-<ACCOUNT_ID>.json` to enable the user to label transactions and set masking status for sensitive transaction descriptions.

`WARNING`: do not edit the description, date or amount fields. These are used by this script to determine whether a transaction has already been downloaded and modifying these values may result in duplicated transactions.

### Uploading Transactions

To upload the transactions stored in the holding (`holding-<ACCOUNT_ID>.json`) files, run `node upload.js`. Transactions will not be uploaded for accounts that do not have a budgie account ID configured.
