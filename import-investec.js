// load credentials from .env file
require('dotenv').config();
const INVESTEC_CLIENT_ID = process.env.INVESTEC_CLIENT_ID;
const INVESTEC_SECRET = process.env.INVESTEC_SECRET;
const DEFAULT_MASKING = process.env.DEFAULT_MASKING ? process.env.DEFAULT_MASKING.toUpperCase() == "TRUE" : false;

if (!INVESTEC_CLIENT_ID || INVESTEC_CLIENT_ID.length == 0) {
    console.log(`investec credentials not found, aborting`);
    process.exit(1);
}

let accountsConfig = require('./accounts.json');

base64 = require('base-64');
const fetch = require('node-fetch');
const fs = require('fs');
const moment = require('moment');
const uuid = require('uuid').v4;

let investecOpenApi = 'https://openapi.investec.com';

let reformat = (investecDate) => {
    return moment(investecDate, 'YYYY-MM-DD').format('YYYYMMDD');
}

let convertAmount = (investecAmount) => {
    return Math.round(investecAmount * 100);
}

// get auth token
fetch(`${investecOpenApi}/identity/v2/oauth2/token`, {
    method: 'post',
    body:    'grant_type=client_credentials&scope=accounts',
    headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': `Basic ${base64.encode(INVESTEC_CLIENT_ID + ":" + INVESTEC_SECRET)}`
    },
})
.then(res => res.json())
.then((authResponse) => {
    console.log(`auth success: ${JSON.stringify(authResponse)}`);
    let token = authResponse.access_token;

    // download the transactions since the latest date
    for (let accountId in accountsConfig) {
        let account = accountsConfig[accountId];
        if (account.bank != "investec") {
            console.log(`${accountId} is not an investec account, skipping.`);
            continue;
        }
        if (!account.budgieAccountId) {
            console.log(`${accountId} does not have a "budgieAccountId" set, skipping.`);
            continue;
        }
        console.log(`downloading transactions from ${accountId} for ${account.budgieAccountId}...`);
        let fromDate = account.lastUpload ? `?fromDate=${account.lastUpload}` : '';

        // TODO: what happens if a transaction if from a few days prior but the posting date is
        //      today? will today's fromDate pick up the transaction?
        fetch(`${investecOpenApi}/za/pb/v1/accounts/${accountId}/transactions${fromDate}`, {
            headers: {
                'Authorization': `Bearer ${token}`
            },
        })
        .then(res => res.json())
        .then(response => {
            // load holding file if it exists
            let holdingFilename = `./holding-${account.budgieAccountId}.json`;
            let holdingTransactions = [];
            if (fs.existsSync(holdingFilename)) {
                let holding = require(holdingFilename);
                holdingTransactions = holding[account.budgieAccountId];
                console.log(`loaded ${holdingTransactions.length} holding transactions from ${holdingFilename}`);
            }

            let transactions = response.data.transactions;
            console.log(`loaded ${transactions.length} transactions from the investec api`);
            let uploadableBody = {};
            uploadableBody[account.budgieAccountId] = [];
            let uploadableTransactions = uploadableBody[account.budgieAccountId];
            for (let t in transactions) {
                let transaction = transactions[t];
                // update amount sign based on whether it's debit/credit
                // for budgie, positive = debit and negative = credit
                if (transaction.type == "CREDIT") {
                    transaction.amount = 0 - transaction.amount;
                }
                // check holding for matching transaction
                for (let h in holdingTransactions) {
                    let entry = holdingTransactions[h];
                    if (entry && entry.transactionDate == reformat(transaction.transactionDate) &&
                        entry.description == transaction.description &&
                        entry.amount == convertAmount(transaction.amount)) {
                        // copy processed values from holding
                        transaction.transactionId = entry.transactionId;
                        transaction.currency = entry.currency;
                        transaction.labels = entry.labels;
                        transaction.masked = entry.masked;
                        // nullify entry from holding to enable repeat transactions
                        holdingTransactions[h] = null;
                    }
                }
                uploadableTransactions.push({
                    transactionId: transaction.transactionId || uuid(),
                    transactionDate: reformat(transaction.transactionDate),
                    description: transaction.description,
                    amount: convertAmount(transaction.amount),
                    currency: transaction.currency || "ZAR",
                    labels: transaction.labels || [],
                    masked: transaction.masked || DEFAULT_MASKING
                });
            }
            // push all unmatched transactions from holding
            for (let h in holdingTransactions) {
                if (holdingTransactions[h]) {
                    uploadableTransactions.push(holdingTransactions[h]);
                }
            }

            let holding = {};
            holding[account.budgieAccountId] = uploadableTransactions;
            // (re)write holding file
            fs.writeFileSync(holdingFilename, JSON.stringify(holding, null, 4));
            // reload accounts config
            accountsConfig = require('./accounts.json');
            // update last upload date
            accountsConfig[accountId].lastUpload =  moment().format('YYYY-MM-DD');
            // rewrite accounts config
            fs.writeFileSync('./accounts.json', JSON.stringify(accountsConfig, null, 4));
        });
    }
});




