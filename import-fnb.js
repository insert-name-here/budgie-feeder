// load credentials from .env file
require('dotenv').config();
const DEFAULT_MASKING = process.env.DEFAULT_MASKING ? process.env.DEFAULT_MASKING.toUpperCase() == "TRUE" : false;

const fs = require('fs');
const moment = require('moment');
const uuid = require('uuid').v4;

let accountsConfig = require('./accounts.json');

const SUPPORTED_FILE_TYPES = ["csv"];

function showHelp() {
    console.log(
`
usage:
    node import-fnb.js <FILE_NAME>
`
    );
}

// validate arguments
var args = process.argv.slice(2);
let csvFile;

while (args.length > 0) {
    let arg = args.splice(0, 1)[0];
    switch (arg) {
        case "--help":
        case "-h":
            showHelp();
            process.exit(0);
            break;
        default:
            if (csvFile) {
                console.error(`unrecognized argument '${arg}'`);
                showHelp();
                process.exit(1);
            }
            csvFile = arg;
            console.log(`csv file: ${csvFile}`);
            break;
    }
}

if (!csvFile) {
    console.error(`missing required arguments`);
    showHelp();
    process.exit(1);
}

let reformat = (fnbDate, year) => {
    // can't be sure if '12 Mar' or 'Mar 12'
    let result;
    result = fnbDate.match(/^\d{2} [A-Za-z]{3}$/);
    if (result) {
        return moment(`${fnbDate} ${year}`, 'DD MMM YYYY').format('YYYYMMDD');
    }
    result = fnbDate.match(/^[A-Za-z]{3} \d{2}$/);
    if (result) {
        return moment(`${fnbDate} ${year}`, 'MMM DD YYYY').format('YYYYMMDD');
    }
    throw new Error(`unrecognized date format: ${fnbDate}`);
}

let convertAmount = (fnbAmount) => {
    return Math.round(fnbAmount * 100);
}

// extract the transactions
fs.readFile(csvFile, 'utf8', (err, data) => {
    if (err) {
        console.error(`error reading ${csvFile}`, err);
        process.exit(1);
    }
    // split rows on commas when not surrounded by quotation marks,
    // trim whitespace and strip quotation marks
    let lines = data.split(/\r?\n/).map(
        row => {
            // to avoid splitting a string with a comma, loop through the characters
            // allow quotations inside quotations (if they're a different type)
            let tokens = [''];
            let startQuote = null;
            for (let i = 0; i < row.length; i++) {
                switch (row[i]) {
                    case "\"":
                    case "\'":
                    case "\`":
                        if (!startQuote) {
                            startQuote = row[i];
                        } else {
                            if (startQuote == row[i]) {
                                startQuote = null;
                            } else {
                                // add quote character to current token
                                tokens[tokens.length - 1] += row[i];
                            }
                        }
                        break;
                    case ",":
                        if (startQuote) {
                            // add comma to current token
                            tokens[tokens.length - 1] += row[i];
                        } else {
                            // split token
                            tokens.push('');
                        }
                        break;
                    default:
                        // add character to current token
                        tokens[tokens.length - 1] += row[i];
                }
            }
            return tokens.map(item => item.trim());
        });

    let accountId = lines[0][1];
    let account = accountsConfig[accountId];
    if (!account) {
        console.error(`csv account number ${accountId} not found, aborting.`);
        process.exit(1);
    }
    if (account.bank != "fnb") {
        console.error(`${accountId} is not an fnb account, aborting.`);
        process.exit(1);
    }
    if (!account.budgieAccountId) {
        console.error(`${accountId} does not have a "budgieAccountId" set, aborting.`);
        process.exit(1);
    }
    console.log(`extracting transactions from ${csvFile} for ${account.budgieAccountId}...`);

    // RUSHED HACK: extract year from known row
    // TODO make this make sense
    // 3,3,'18 February 2021','18 March 2021',...
    // but sometimes (often) the previous line has a newline so check first
    let year;
    if (Number(lines[4][0])) {
        year = lines[4][2].substr(-4);
    } else {
        year = lines[5][2].substr(-4);
    }

    let line;
    // the header row: the fnb csv format is inconsistent
    // it's the first row starting with 5 and not followed by nothing
    //  eg. 5
    // and not followed by 'Transactions'
    //  eg. 5,,'Transactions'
    let header;
    while (!header && lines.length > 0) {
        line = lines.splice(0, 1)[0];
        if (line[0] == 5) {
            if (line[1] && line.indexOf('Transactions') < 0) {
                header = line;
            }
        }
    }
    if (!header) {
        console.error(`no transactions found, aborting.`);
        process.exit(1);
    }

    // load holding file if it exists
    let holdingFilename = `./holding-${account.budgieAccountId}.json`;
    let uploadableBody = {};
    uploadableBody[account.budgieAccountId] = [];
    let uploadableTransactions = uploadableBody[account.budgieAccountId];

    // 5,'Number','Date','Description1','Description2','Description3','Amount','Balance','Accrued Charges'
    // 5,'Number','Date','Description','Reference','Amount','Balance','Accrued Charges'
    let transactionsIndex = header[0];
    // initialize line to first transaction line
    for (let line = lines.splice(0, 1)[0];
         line && line[0] === transactionsIndex;
         line = lines.splice(0, 1)[0]
    ) {
        let transaction = {};
        for (let i in header) {
            transaction[header[i]] = line[i];
        };
        // description = description2 / description3
        if (transaction.Description2 && transaction.Description3) {
            transaction.Description = `${transaction.Description2} / ${transaction.Description3}`;
        } else {
            transaction.Description = transaction.Description || transaction.Description2 || transaction.Description3 || transaction.Description1;
        }
        // for budgie, positive = debit and negative = credit (opposite of fnb)
        transaction.Amount = convertAmount(0 - Number(transaction.Amount));
        transaction.Date = reformat(transaction.Date, year);

        if (transaction.Amount != 0) {
            // reformat transaction for budgie and add to uploadables
            uploadableTransactions.push({
                transactionId: uuid(),
                transactionDate: transaction.Date,
                description: transaction.Description,
                amount: transaction.Amount,
                currency: "ZAR",
                labels: [],
                masked: DEFAULT_MASKING,
            });
        }
    }

    // (re)write holding file
    fs.writeFileSync(holdingFilename, JSON.stringify(uploadableBody, null, 4));
    // reload accounts config
    accountsConfig = require('./accounts.json');
    // update last upload date
    accountsConfig[account.accountNumber].lastUpload =  moment().format('YYYY-MM-DD');
    // rewrite accounts config
    fs.writeFileSync('./accounts.json', JSON.stringify(accountsConfig, null, 4));
    console.log(`transactions extracted successfully, please review ${holdingFilename}`);
});
